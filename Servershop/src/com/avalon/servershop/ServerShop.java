package com.avalon.servershop;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.citizensnpcs.api.CitizensPlugin;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import com.avalon.metrics.Metrics;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.avalon.listener.InventoryClick;
import com.avalon.listener.NPCEvents;
import com.avalon.listener.PlayerListener;
import com.avalon.listener.Signs;
import com.avalon.listener.VoteListener;
import com.avalon.token.TokenCommand;
import com.avalon.token.TokenUtils;
import com.avalon.utils.FileUtils;
import com.avalon.utils.util;
import com.avalon.utils.Lang;
import com.avalon.mysql.*;

public class ServerShop extends JavaPlugin {
	public final Logger log = Logger.getLogger("Minecraft");
	public static Economy econ = null;
	public static Permission permission = null;
	public CitizensPlugin npc = null;
	
	
	public static YamlConfiguration LANG;
	public static File LANG_FILE;
	
	
	public Inventory slist;
	
	public static File msg;
	public static FileConfiguration m;
	public static File folder;
	
	public HashMap<Integer,Inventory> shops = new HashMap<Integer,Inventory>();
	public HashMap<String, Integer> ShopSelect = new HashMap<String, Integer>();
	
	public static HashMap<String, String> PermissionShopItems = new HashMap<String, String>();
	public static HashMap<String, Integer> PermissionShopCooldown = new HashMap<String, Integer>();
	public static HashMap<String, Long> PermissionCooldownPlayer = new HashMap<String, Long>();
	
	
	public static HashMap<String, String> CommandShopItems = new HashMap<String, String>();
	public static HashMap<String, Integer> CommandShopCooldown = new HashMap<String, Integer>();
	public static HashMap<String, Long> CommandShopCooldownPlayer = new HashMap<String, Long>();
	
	
	public static HashMap<String, String> PlayerShopItems = new HashMap<String, String>();
	public static HashMap<String, Integer> PlayerShopCooldown = new HashMap<String, Integer>();
	public static HashMap<String, Long> PlayerShopCooldownPlayer = new HashMap<String, Long>();
	
	public ArrayList<String> BlockedCommands = new ArrayList<String>();
	
	
	
	
	public MySQL MySQL = null;
	public Connection c = null;
	public TokenMySQL tokensql;
	public static boolean MySQL_E = false;
	
	public void onEnable() {
		if (!setupEconomy()) {
		getLogger().warning("Could not load plugin due Vault compatible economy plugin installed.");
		getServer().getPluginManager().disablePlugin(this);
		return;	
	} else {
		getLogger().info("Plugin is using " + econ.getName() + " as economy system!");
	}
		
		if (!setupPermissions()) {
		getLogger().warning("Could not load plugin due No Vault compatible permission plugin installed.");
		getServer().getPluginManager().disablePlugin(this);
		return;	
	} else {
		getLogger().info("Plugin is using " + permission.getName() + " as permission system!");
	}
		folder = getDataFolder();
		FileUtils.home = this.getDataFolder();
		TokenUtils.home = this.getDataFolder();
		if (setupNPC()) {
			getLogger().info("Citizens2 found. Activating NPC support");
			getServer().getPluginManager().registerEvents(new NPCEvents(this), this);
		} else {
			getLogger().info("Citizens2 not found.. Skipping NPC support.");
		}
		
		File configlist = new File(this.getDataFolder().getAbsolutePath() + File.separator + "config.yml");
		if (!this.getDataFolder().exists()) {
			this.getDataFolder().mkdir();
		}
		if (!configlist.exists()) {
			FileUtils.setupMainConfig();	
		}
		
		FileUtils.TokenConfig();
		
		FileUtils.setupItemShopConfig();
		this.debug("==========================");
		this.debug("Debug messages activated.");
		this.debug("==========================");
		FileUtils.updateConfig();
		this.reloadConfig();
		createInventory();
		
		if (this.getConfig().getBoolean("main.blockbuyablecommands")) {
		getServer().getPluginManager().registerEvents(new BlockBuyableCommands(this), this);
		log.info("Buyable commands will now be blocked!");
		}
		if (isVotifierEnabled() && this.getConfig().getBoolean("Vote.Enable")) {
			log.info("Found and enabled Votifier support!");
			getServer().getPluginManager().registerEvents(new VoteListener(this), this);
		}
		
		getServer().getPluginManager().registerEvents(new Signs(this), this);
		util.LogsEnabled = getConfig().getBoolean("main.EnableLogs");
		util.pricetype = Material.getMaterial(getConfig().getString("main.Item_Currency").toUpperCase().replace(' ', '_'));
		getServer().getPluginManager().registerEvents(new InventoryClick(this), this);
		getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
		
		getCommand("shop").setExecutor(new ShopCommand(this));
		if (this.getConfig().getBoolean("main.EnableToken")) {
		getCommand("tokens").setExecutor(new TokenCommand(this));
		}
		this.loadLang();
		
		
		if (this.getConfig().getBoolean("main.EnableMetrics")) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "Thanks for using enabled Metrics support :)");
			try {
			    Metrics metrics = new Metrics(this);
			    metrics.start();
			} catch (IOException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Oh noes :( Metrics failed to submint stats :(");
			}
		} else {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Oh noes :( You've disabled Metrics. Consider to enable it to help the developer!");
		}
		
		
		// Playing arround with MySQL 
		
		if (this.getConfig().getBoolean("main.EnableMySQL")) {
			
			String user = this.getConfig().getString("mysql.User");
		    String database = this.getConfig().getString("mysql.Database");
		    String password = this.getConfig().getString("mysql.Password");
		    String port = this.getConfig().getString("mysql.Port");
		    String hostname = this.getConfig().getString("mysql.Hostname");
			
			
			MySQL = new MySQL(this, hostname, port, database, user, password);
			
			try {
				c = MySQL.openConnection();
	            c.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS `ServerShop` (`ID`  int NULL AUTO_INCREMENT ,`Nickname`  varchar(16) NULL ,`Token`  int NULL ,PRIMARY KEY (`ID`));");
				
				
			} catch (Exception ex) {
				System.out.println("Connection to the database failed.");
				System.out.println(ex.getMessage());
			}
			tokensql = new TokenMySQL(this);
			MySQL_E = true;
			log.info("Using MySQL for Token Storage.");
		} else {
			log.info("Using FLATFILE for Token Storage.");
		}
	}

	public void onDisable() {
	}
	
	  private boolean setupEconomy() {
		    if (getServer().getPluginManager().getPlugin("Vault") == null) {
		      return false;
		    }
		    RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
		    if (rsp == null) {
		      return false;
		    }
		    econ = (Economy)rsp.getProvider();
		    return econ != null;
		  }
	  
	  private boolean setupPermissions() {
		    RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
		    permission = (Permission)rsp.getProvider();
		    return permission != null;
		  }
	  
	  
		private boolean setupNPC() {
			if (getServer().getPluginManager().getPlugin("Citizens") == null) {
				return false;
			} else {
				npc = (CitizensPlugin)getServer().getPluginManager().getPlugin("Citizens");
				return true;
			}
		}
		
		private boolean isVotifierEnabled() {
		    return getServer().getPluginManager().getPlugin("Votifier") != null;
		  }
			  
	 // END	
	  
	  
	  /*
	   * 
	   * Create and fill the shop selection Inventory.
	   * 
	   */
	  
	  public void createInventory() {
			ConfigurationSection ShopList = this.getConfig().getConfigurationSection("list");
			for (String key : ShopList.getKeys(false))
			{
				ConfigurationSection item = ShopList.getConfigurationSection(key);
				int id = Integer.parseInt(key);
				String Shopname = ChatColor.translateAlternateColorCodes('&', item.getString("name"));
				Inventory tmp = Bukkit.createInventory(null, item.getInt("size"), Shopname);
				this.debug("Added Shop to Hashmap with ID " + id);
				shops.put(id, tmp);
				String ShopTitel  = ChatColor.stripColor(Shopname);
				this.ShopSelect.put(ShopTitel, id);
				
				if (item.getBoolean("returnbutton")) {
				ItemStack ReturnButton = new ItemStack(Material.REDSTONE_BLOCK, 1);
				ItemMeta Meta = ReturnButton.getItemMeta();
				Meta.setDisplayName(ChatColor.YELLOW + "Return");
				Meta.setLore(Arrays.asList(ChatColor.GRAY + "Return to", ChatColor.GRAY + "the shop selection."));
				ReturnButton.setItemMeta(Meta);
				tmp.setItem(tmp.getSize() -1, ReturnButton);
				this.debug("Added Return button to Inventory with ID " + id);
				AddItemToShops(ShopTitel);
				} else {
					AddItemToShops(ShopTitel);
				}
				
			}
			File configlist = new File(this.getDataFolder().getAbsolutePath() + File.separator + "config.yml");
			FileConfiguration cfg = YamlConfiguration.loadConfiguration(configlist);
			int slots = cfg.getInt("main.slots");
			slist = Bukkit.createInventory(null, slots, "Shop");
			
			ConfigurationSection ShopListAdd = cfg.getConfigurationSection("list");
			
			for (String key : ShopListAdd.getKeys(false))
			{
				ConfigurationSection item = ShopListAdd.getConfigurationSection(key);
				String n = ChatColor.translateAlternateColorCodes('&', item.getString("name"));
				String reward = item.getString("icon").replace(':', ' ');
				String[] r = reward.split(" ");
				
				Material mat = Material.getMaterial(r[0].toUpperCase());
				int SubID;
				
				
				if (r.length == 1) {
					SubID = 0;
				} else {
					SubID = Integer.parseInt(r[1]);
				}
				if (mat == null) {
					this.log.info("=======================================================================================");
					this.log.warning("[ERROR] Material Name NOT found for " + item.getString("icon").toUpperCase().replace(' ', '_'));
					this.log.warning("[ERROR] Check the official bukkit API documentation for a full material list");
					this.log.warning("[ERROR] http://jd.bukkit.org/beta/apidocs/org/bukkit/Material.html");
					this.log.info("=======================================================================================");
					continue;
				}

			ItemStack i = new ItemStack(mat,1, (short) SubID);
	    	ItemMeta Meta = i.getItemMeta();
	    	
	    	
	        List<String> description = new ArrayList<String>();			
	        for (String sl : item.getStringList("description")) {
	        	description.add(ChatColor.translateAlternateColorCodes('&', sl));
	        }
	    	Meta.setDisplayName(n);
	    	Meta.setLore(description);
	    	i.setItemMeta(Meta);
	    	slist.setItem(Integer.parseInt(key), i);
			}		
		}
	  
	  
	  
	  
	  
	  /*
	   * 
	   * Load a shop.yml with the given name and setup/fill inventories.
	   * 
	   */
	  
	  private String PriceToString(Double price){
		  	String BuyPrice;
			if (price == 0) {
				BuyPrice = ChatColor.GREEN + "Free";
			} 
			
			else if (price == -1) {
				BuyPrice = ChatColor.RED + "Unable to be sold.";
			} else {
				BuyPrice = String.valueOf(price);
			}
			return BuyPrice;
	  }
	  
	  private String SellToString(Double sell) {
		  String SellPrice;
		  
			if (sell == 0) {
				SellPrice = ChatColor.GREEN + "Free";
			} 
			
			else if (sell == -1) {
				SellPrice = ChatColor.RED + "Unable to be sold.";
			} else {
				SellPrice = String.valueOf(sell);
			}
		  return SellPrice;
	  }
	  
	  public void AddItemToShops(String name) {
			File configlist = new File(this.getDataFolder().getAbsolutePath() + File.separator + "shops" + File.separator + name + ".yml");
			FileConfiguration cfg = YamlConfiguration.loadConfiguration(configlist);
			ConfigurationSection wareSection = cfg.getConfigurationSection("stock");
			
			if (!configlist.exists()) {
				try {
					configlist.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			if (wareSection == null) {
				this.log.warning("[Shop] File '" + configlist.getName() + "' is emtpy. Cannot add items to Inventory.");
				return;
			}
			
			for (String key : wareSection.getKeys(false))
			{
				ConfigurationSection item = wareSection.getConfigurationSection(key);
				int position = Integer.parseInt(key);
				debug("Inventory Position: " + key);
				String type = item.getString("Type");
				
				
				/*
				 * 
				 * Setup normal items.
				 * 
				 */
				
				
				if (type.equalsIgnoreCase("Item")) {
					   String reward = item.getString("Reward").replace(':', ' ');	
					   int amount = item.getInt("Amount");	
					   if (createItemStack(reward, amount) == null) {
						   continue;
					   }
					
					   ItemStack add = createItemStack(reward,amount);
					
					
						Inventory tmp = this.shops.get(this.ShopSelect.get(name));
			        
						ItemMeta Meta = add.getItemMeta();
						
						
						String displayname = item.getString("Displayname");
						if (displayname != null) {
							displayname = ChatColor.translateAlternateColorCodes('&', displayname);
						}
						Meta.setDisplayName(displayname);
						
						
						
				        List<String> lore = new ArrayList<String>();
						lore.add(ChatColor.GREEN + "Type: " + type);
						lore.add("");
						if (item.isSet("Discount")) {
							int discount = item.getInt("Discount");
							float endbetrag = util.calculateDiscount((float)item.getDouble("Price"), discount, "endbetrag");	
							lore.add(ChatColor.YELLOW + "Price: " + endbetrag + " / " + SellToString(item.getDouble("Sell")));
						} else {
						   lore.add(ChatColor.YELLOW + "Price: " + PriceToString(item.getDouble("Price")) + " / " + SellToString(item.getDouble("Sell")));
						}
						
						lore.add(ChatColor.YELLOW + "Pricetype: " + item.getString("Pricetype"));
						if (item.isSet("Discount")) {
							int discount = item.getInt("Discount");
							lore.add("");
							lore.add(ChatColor.BOLD + "" + ChatColor.AQUA + "On Sale!!");
							lore.add(ChatColor.GREEN + "Save now " + discount + "% upon purchase! ");
							lore.add(ChatColor.GREEN + "" + util.calculateDiscount((float)item.getDouble("Price"), discount, "endbetrag") + ChatColor.AQUA  + " instead of " + ChatColor.RED + ChatColor.STRIKETHROUGH + PriceToString(item.getDouble("Price")));
						}
						lore.add("");
				        
						if (item.isSet("lore")) {
						
				        for (String sl : item.getStringList("lore")) {
				          lore.add(ChatColor.translateAlternateColorCodes('&', sl));
				        }

			        }
			        
				        Meta.setLore(lore);
						add.setItemMeta(Meta);
						lore.clear();
					 

					if (item.isSet("Enchantments")) {
						String ench = item.getString("Enchantments");
						for(String e : ench.split(";")) {
							Enchantment en = Enchantment.getByName(e.split(":")[0]);
							if (en != null) {
							add.addUnsafeEnchantment(en, Integer.valueOf(e.split(":")[1]));
							} else {
								this.log.warning("[ERROR] - Enchantment not found (" + e.split(":")[0] + ")" );
								continue;
							}
						}
					}
					tmp.setItem(position, add);
				}
				
				
				/*
				 * 
				 * Setup Permission Node Items.
				 * 
				 */
				
				
				else if (type.equalsIgnoreCase("Permission")) {
					String reward = item.getString("Reward").replace(':', ' ');	
					   int amount = 1;	
					   if (createItemStack(reward, amount) == null) {
						   continue;
					   }
					
					   ItemStack add = createItemStack(reward,amount);
					
					
					String DisplayName = ChatColor.translateAlternateColorCodes('&', item.getString("Displayname"));
				    String rawDisplayName = ChatColor.stripColor(DisplayName);
					if (DisplayName == null) {
						this.log.warning("[ERROR] Displayname cant be null!");
						continue;
					}
					if (PermissionShopItems.containsKey(name + rawDisplayName)) {
						this.log.warning("[ERROR] You cannot add multiple items with the same displayname in Permission Shops (" + DisplayName + ")" );
						continue;
					}
					PermissionShopItems.put(name + rawDisplayName, item.getString("Permission"));
					
					Inventory tmp = this.shops.get(this.ShopSelect.get(name));
					ItemMeta Meta = add.getItemMeta();
					Meta.setDisplayName(DisplayName);
			        List<String> lore = new ArrayList<String>();
			        lore.add(ChatColor.GREEN + "Type: " + type);
					lore.add("");
					if (item.isSet("Discount")) {
						int discount = item.getInt("Discount");
						float endbetrag = util.calculateDiscount((float)item.getDouble("Price"), discount, "endbetrag");	
						lore.add(ChatColor.YELLOW + "Price: " + endbetrag);
					} else {
					   lore.add(ChatColor.YELLOW + "Price: " + PriceToString(item.getDouble("Price")));
					}
					
					lore.add(ChatColor.YELLOW + "Pricetype: " + item.getString("Pricetype"));
					if (item.isSet("Discount")) {
						int discount = item.getInt("Discount");
						lore.add("");
						lore.add(ChatColor.BOLD + "" + ChatColor.AQUA + "On Sale!!");
						lore.add(ChatColor.GREEN + "Save now " + discount + "% upon purchase! ");
						lore.add(ChatColor.GREEN + "" + util.calculateDiscount((float)item.getDouble("Price"), discount, "endbetrag") + ChatColor.AQUA  + " instead of " + ChatColor.RED + ChatColor.STRIKETHROUGH + PriceToString(item.getDouble("Price")));
					}
					lore.add("");
					
					if (item.isSet("Cooldown")) {
						int delay = item.getInt("Cooldown");
						PermissionShopCooldown.put(name + rawDisplayName, delay);
					}
			        
					if (item.isSet("lore")) {
					
			        for (String sl : item.getStringList("lore")) {
			          lore.add(ChatColor.translateAlternateColorCodes('&', sl));
			        }

		        }
		        
			        Meta.setLore(lore);
					add.setItemMeta(Meta);
					tmp.setItem(position, add);
				}
				
				/*
				 * 
				 * Setup Console Command Items.
				 * 
				 */
				
				
				else if (type.equalsIgnoreCase("Command")) {
					   String reward = item.getString("Reward").replace(':', ' ');	
					   int amount = 1;	
					   if (createItemStack(reward, amount) == null) {
						   continue;
					   }
					
					   ItemStack add = createItemStack(reward,amount);
					

					   String DisplayName = ChatColor.translateAlternateColorCodes('&', item.getString("Displayname"));
					    String rawDisplayName = ChatColor.stripColor(DisplayName);
					if (DisplayName == null) {
						this.log.warning("[ERROR] Displayname cant be null!");
						continue;
					}
					
					if (CommandShopItems.containsKey(name + rawDisplayName)) {
						this.log.warning("[ERROR] You cannot add multiple items with the same displayname in Command Shops (" + DisplayName + ")" );
						continue;
					}
					CommandShopItems.put(name + rawDisplayName, item.getString("Command"));

					Inventory tmp = this.shops.get(this.ShopSelect.get(name));
					ItemMeta Meta = add.getItemMeta();
					Meta.setDisplayName(DisplayName);
					List<String> lore = new ArrayList<String>();
					lore.add(ChatColor.GREEN + "Type: " + type);
					lore.add("");
					if (item.isSet("Discount")) {
						int discount = item.getInt("Discount");
						float endbetrag = util.calculateDiscount((float)item.getDouble("Price"), discount, "endbetrag");	
						lore.add(ChatColor.YELLOW + "Price: " + endbetrag);
					} else {
					   lore.add(ChatColor.YELLOW + "Price: " + PriceToString(item.getDouble("Price")));
					}
					
					lore.add(ChatColor.YELLOW + "Pricetype: " + item.getString("Pricetype"));
					if (item.isSet("Discount")) {
						int discount = item.getInt("Discount");
						lore.add("");
						lore.add(ChatColor.BOLD + "" + ChatColor.AQUA + "On Sale!!");
						lore.add(ChatColor.GREEN + "Save now " + discount + "% upon purchase! ");
						lore.add(ChatColor.GREEN + "" + util.calculateDiscount((float)item.getDouble("Price"), discount, "endbetrag") + ChatColor.AQUA  + " instead of " + ChatColor.RED + ChatColor.STRIKETHROUGH + PriceToString(item.getDouble("Price")));
					}
					lore.add("");
					
					if (item.isSet("Cooldown")) {
						int delay = item.getInt("Cooldown");
						CommandShopCooldown.put(name + rawDisplayName, delay);
					}
			        
					if (item.isSet("lore")) {
					
			        for (String sl : item.getStringList("lore")) {
			          lore.add(ChatColor.translateAlternateColorCodes('&', sl));
			        }

		        }
					Meta.setLore(lore);
					add.setItemMeta(Meta);
					tmp.setItem(position, add);	
				}
				
				/*
				 * 
				 * Setup Player Command Items.
				 * 
				 */
				
				else if (type.equalsIgnoreCase("PlayerCommand")) {
					String reward = item.getString("Reward").replace(':', ' ');	
					   int amount = 1;	
					   if (createItemStack(reward, amount) == null) {
						   continue;
					   }
					
					   ItemStack add = createItemStack(reward,amount);
					
					
					String DisplayName = ChatColor.translateAlternateColorCodes('&', item.getString("Displayname"));
					String rawDisplayName = ChatColor.stripColor(DisplayName);
					if (DisplayName == null) {
						this.log.warning("Displayname cant be null!");
						continue;
					}

					if (PlayerShopItems.containsKey(name + rawDisplayName)) {
						this.log.warning("[ERROR] You cannot add multiple items with the same displayname in Command Shops (" + DisplayName + ")" );
						continue;
					}
					PlayerShopItems.put(name + rawDisplayName, item.getString("Command"));
					BlockedCommands.add(item.getString("Command"));
					
					Inventory tmp = this.shops.get(this.ShopSelect.get(name));
					ItemMeta Meta = add.getItemMeta();
					Meta.setDisplayName(DisplayName);
			        List<String> lore = new ArrayList<String>();
			        lore.add(ChatColor.GREEN + "Type: " + type);
					lore.add("");
					if (item.isSet("Discount")) {
						int discount = item.getInt("Discount");
						float endbetrag = util.calculateDiscount((float)item.getDouble("Price"), discount, "endbetrag");	
						lore.add(ChatColor.YELLOW + "Price: " + endbetrag);
					} else {
					   lore.add(ChatColor.YELLOW + "Price: " + PriceToString(item.getDouble("Price")));
					}
					
					lore.add(ChatColor.YELLOW + "Pricetype: " + item.getString("Pricetype"));
					if (item.isSet("Discount")) {
						int discount = item.getInt("Discount");
						lore.add("");
						lore.add(ChatColor.BOLD + "" + ChatColor.AQUA + "On Sale!!");
						lore.add(ChatColor.GREEN + "Save now " + discount + "% upon purchase! ");
						lore.add(ChatColor.GREEN + "" + util.calculateDiscount((float)item.getDouble("Price"), discount, "endbetrag") + ChatColor.AQUA  + " instead of " + ChatColor.RED + ChatColor.STRIKETHROUGH + PriceToString(item.getDouble("Price")));
					}
					lore.add("");
					
					if (item.isSet("Cooldown")) {
						int delay = item.getInt("Cooldown");
						PlayerShopCooldown.put(name + rawDisplayName, delay);
					}
			        
					if (item.isSet("lore")) {
					
			        for (String sl : item.getStringList("lore")) {
			          lore.add(ChatColor.translateAlternateColorCodes('&', sl));
			        }

		        }
					Meta.setLore(lore);
					add.setItemMeta(Meta);
					tmp.setItem(position, add);	
				}	
			}
	  }
	  /*
	   * 
	   * Log Debug Messages.
	   * 
	   */

	public void debug(String msg){
		  if (this.getConfig().getBoolean("main.Debug"))
		  this.log.info("[Debug] " + msg );  
	  }
	  
	  
	  public void reloadShops() {
			for (Entry<Integer, Inventory> ntc : shops.entrySet()) {
				ntc.getValue().clear();
			}
			PlayerShopItems.clear();
			CommandShopItems.clear();
			PermissionShopItems.clear();
			slist.clear();
			createInventory();
	  }
	  
	  
		private boolean isInt(String s) {
		    try { 
		        Integer.parseInt(s); 
		    } catch(NumberFormatException e) { 
		        return false; 
		    }
		    return true;
		}
		
		  private ItemStack createItemStack(String reward, int amount) {
				String[] r = reward.split(" ");
				
				Material mat = null;
				int SubID = 0;
				
				
				if (this.isInt(r[0])) {
					int id = Integer.parseInt(r[0]);
					 mat = Material.getMaterial(id);
				} 
				else { mat = Material.getMaterial(r[0].toUpperCase()); }
				
				
				if (r.length != 1) SubID = Integer.parseInt(r[1]);
				
				if (mat == null) {
					sendConsoleMessage(ChatColor.GOLD + "=======================================================================================");
					sendConsoleMessage(ChatColor.RED + "[ERROR] Material Name NOT found for " + reward.toUpperCase().replace(' ', '_'));
					sendConsoleMessage(ChatColor.RED + "[ERROR] Check the official bukkit API documentation for a full material list");
					sendConsoleMessage(ChatColor.RED + "[ERROR] http://jd.bukkit.org/beta/apidocs/org/bukkit/Material.html");
					sendConsoleMessage(ChatColor.GOLD + "=======================================================================================");
					return null;
				}

				if (amount > 64 || amount == 0) {
					sendConsoleMessage(ChatColor.RED + "Amount of '" + r[0].toUpperCase() + "' cannot be " + amount + ".");
					return null;
				}
				
				
				ItemStack item = new ItemStack(mat, amount, (short) SubID);
				return item;
			}
		  
		  public void sendConsoleMessage(String message) {
			  Bukkit.getConsoleSender().sendMessage("[ServerShop-GUI] " + message);
			  return;
		  }
		  
		  /**
		   * Load the lang.yml file.
		   * @return The lang.yml config.
		   */
		  public void loadLang() {
		      File lang = new File(getDataFolder(), "lang.yml");
		      if (!lang.exists()) {
		          try {
		              getDataFolder().mkdir();
		              lang.createNewFile();
		              InputStream defConfigStream = this.getResource("lang.yml");
		              if (defConfigStream != null) {
		                  YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
		                  defConfig.save(lang);
		                  Lang.setFile(defConfig);
		                  return;
		              }
		          } catch(IOException e) {
		              e.printStackTrace(); // So they notice
		              log.severe("[PluginName] Couldn't create language file.");
		              log.severe("[PluginName] This is a fatal error. Now disabling");
		              this.setEnabled(false); // Without it loaded, we can't send them messages
		          }
		      }
		      YamlConfiguration conf = YamlConfiguration.loadConfiguration(lang);
		      for(Lang item:Lang.values()) {
		          if (conf.getString(item.getPath()) == null) {
		              conf.set(item.getPath(), item.getDefault());
		          }
		      }
		      Lang.setFile(conf);
		      this.LANG = conf;
		      this.LANG_FILE = lang;
		      try {
		          conf.save(getLangFile());
		      } catch(IOException e) {
		          log.log(Level.WARNING, "PluginName: Failed to save lang.yml.");
		          log.log(Level.WARNING, "PluginName: Report this stack trace to <your name>.");
		          e.printStackTrace();
		      }
		  }
		  
		  /**
		  * Gets the lang.yml config.
		  * @return The lang.yml config.
		  */
		  public YamlConfiguration getLang() {
		      return LANG;
		  }
		   
		  /**
		  * Get the lang.yml file.
		  * @return The lang.yml file.
		  */
		  public File getLangFile() {
		      return LANG_FILE;
		  }
		  
			public boolean checkConn() {
				try {
					if (c.isClosed()) {
						c = MySQL.openConnection();
						return false;
					} else {
						return true;
					}
				} catch (SQLException e) {
					System.out.println(e.getMessage());
					return false;
				}
			}
}
