package com.avalon.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.milkbowl.vault.economy.EconomyResponse;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.avalon.mysql.TokenMySQL;
import com.avalon.servershop.ServerShop;
import com.avalon.token.TokenUtils;

public class util {
	
	  public static boolean LogsEnabled;	
	  public static Material pricetype;
	
	  
	    /**
	    * Check if the player has a free slot in his Inventory.
	    * @param player The Player object.
	    */  
	public static boolean hasSpot(Player player) {
		 int items = player.getInventory().firstEmpty();
		 if (items == -1) {
			 player.sendMessage(Lang.FULL_INVENTORY.toString());
			 player.closeInventory();
			 return false; 
		 }
		 return true;	 
	}
	
		/**
	    * Take money from the player, returns false and send a message if the player has not enough money.
	    * @param player The Player object.
	    * @param money the amount of money that should be taken.
	    */
	public static boolean payMoney(Player player, float money) {
		EconomyResponse r = ServerShop.econ.withdrawPlayer(player.getName(), money);
		if (r.transactionSuccess()) {
			player.sendMessage(Lang.PURCHASE_MONEY_COMPLETE.toString().replace("%amount%", ""+money).replace("%currencyname%", ServerShop.econ.currencyNameSingular()));
			return true;
		}
		else {
			player.sendMessage(Lang.NO_MONEY.toString());
			return false;
		}
	}
	
	
		/**
	    * Take Token from the player, returns false and send a message if the player has not enough Token.
	    * @param player The Player object.
	    * @param money the amount of money that should be taken.
	    */
	public static boolean payToken(Player p, int amount) {
		
		if (ServerShop.MySQL_E) {
		
		if (TokenMySQL.takeToken(p.getName(), amount)) {
			p.sendMessage(Lang.PURCHASE_TOKEN_COMPLETE.toString().replace("%amount%", ""+amount));	
		return true;
		} else {
			p.sendMessage(Lang.NO_TOKEN.toString());
			return false;
		}
		} else {
			if (TokenUtils.takeToken(p.getName(), amount)) {
				p.sendMessage(Lang.PURCHASE_TOKEN_COMPLETE.toString().replace("%amount%", ""+amount));	
			return true;
			} else {
				p.sendMessage(Lang.NO_TOKEN.toString());
				return false;
			}
		}
	}
	
	
		/**
	    * Give the player some Tokens.
	    * @param player The Player object.
	    * @param money the amount of money that should be taken.
	    */
	public static boolean giveToken(Player p, int amount) {
		if (ServerShop.MySQL_E) {
		TokenMySQL.giveToken(p.getName(), amount);
		return true;
		} 
		TokenUtils.giveToken(p.getName(), amount);
		return true;
	}
	
		/**
	    * Take Items from the player, returns false and send a message if the player has not enough Items.
	    * @param player The Player object.
	    * @param money the amount of money that should be taken.
	    */
	public static boolean payitem(Player p, int money) {
		ItemStack i = new ItemStack(pricetype);
		if (p.getInventory().containsAtLeast(i, money)) {
		removeItem(p.getInventory(), pricetype, money);	
	    return true;
		} else {
			p.sendMessage(Lang.NO_ITEMS.toString().replace("%item%", pricetype.toString().toLowerCase().replace("_", " ")));
			return false;
		}
	}
	
	
		/**
	    * Give Items to the player, returns false and send a message if the player has not enough Inventory space.
	    * @param player The Player object.
	    * @param money the amount of money that should be taken.
	    */
	public static boolean giveItem(Player p, int money) {
		if (hasSpot(p)) {
			p.getInventory().addItem(new ItemStack(pricetype, money));
			p.sendMessage(ChatColor.GREEN + "Got " + money + " " + pricetype.name().toLowerCase());
			return true;
		} else {
			p.sendMessage(Lang.FULL_INVENTORY.toString());
			return false;
		}
	}
	
	public static boolean payEXP(Player p, int exp) {
		
		if (p.getLevel() >= exp) {
			
			p.setLevel(p.getLevel() - exp);
			p.playSound(p.getLocation(), Sound.LEVEL_UP, 5, 5);
			p.sendMessage(Lang.PURCHASE_LEVEL_COMPLETE.toString().replace("%level%", ""+exp));
			return true;
			
		} else {
			p.sendMessage(Lang.NO_LEVEL.toString());
			return false;
		}
	}

	
	
	
	public static boolean giveMoney(Player player, float money) {
		EconomyResponse r = ServerShop.econ.depositPlayer(player.getName(), money);
		if (r.transactionSuccess()) {
			player.sendMessage(Lang.SELL_MONEY_COMPLETE.toString().replace("%amount%", ""+money).replace("%currencyname%", ServerShop.econ.currencyNameSingular()));
			return true;
		}
		else {
			player.sendMessage(ChatColor.RED + "Error while adding money to your Account");
			return false;
		}
	}
	
	
	
	public static boolean giveEXP(Player p, int exp) {
		int current = p.getLevel();
		p.setLevel(current + exp);
		p.playSound(p.getLocation(), Sound.LEVEL_UP, 5, 1);
		p.sendMessage(Lang.SELL_LEVEL_COMPLETE.toString().replace("%level%", ""+ exp));
		return true;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static float calculateDiscount(float Betrag, float Discount, String type) {
		  float betragWert = Betrag;
		  float rabattWert = Discount;
		  
		  
		  float rabattFaktor = rabattWert / 100;
		  float betragDifferenz = betragWert * rabattFaktor;
		  float endBetrag = betragWert - betragDifferenz;

		  if (type.equalsIgnoreCase("endbetrag")) {
			  return endBetrag;
		  }
		  else if (type.equalsIgnoreCase("differenz")) {
			  return betragDifferenz;
		  }
		  else {
			  return Betrag;
		  }
	}
	
	 private static int removeItem(Inventory inventory, Material mat, int quantity) {
	        int rest = quantity;
	        for( int i = 0 ; i < inventory.getSize() ; i++ ){
	            ItemStack stack = inventory.getItem(i);
	            if( stack == null || stack.getType() != mat )
	                continue;
	            if( rest >= stack.getAmount() ){
	                rest -= stack.getAmount();
	                inventory.clear(i);
	            } else if( rest>0 ){
	                    stack.setAmount(stack.getAmount()-rest);
	                    rest = 0;
	            } else {
	                break;
	            }
	        }
	        return quantity-rest;
	    }
		public static void logToFile(String message, String file)
	    {
			
			if (LogsEnabled) {
			
	        try
	        {
	            File dataFolder = ServerShop.folder;
	            File logFolder = new File(ServerShop.folder.getAbsolutePath() + File.separator + "logs" + File.separator);
	            if(!dataFolder.exists())
	            {
	                dataFolder.mkdir();
	            }
	            if(!logFolder.exists())
	            {
	            	logFolder.mkdir();
	            } 
	            File saveTo = new File(ServerShop.folder.getAbsolutePath() + File.separator + "logs" + File.separator +  file);
	            if (!saveTo.exists())
	            {
	                saveTo.createNewFile();
	            }
	            FileWriter fw = new FileWriter(saveTo, true);
	            PrintWriter pw = new PrintWriter(fw);
				Date dt = new Date();
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				String time = df.format(dt);
	            pw.println("[" + time + "] " + message);
	            pw.close();
	 
	        } catch (IOException e)
	        {
	            e.printStackTrace();
	        }
			} else {
				return;
			}
	 
	    }
}
