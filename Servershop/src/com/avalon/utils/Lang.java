package com.avalon.utils;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
 
/**
* An enum for requesting strings from the language file.
* @author gomeow
*/
public enum Lang {
    TITLE("title-name", "&4[&fServerShop&4]:"),
    NO_PERMISSION("NO_PERMISSION", "&cYou lack the proper permission for this action."),
    RELOAD("RELOAD", "&aServershop reloaded."),
    NPC_SPAWNED("NPC_SPAWNED", "&aNPC has been spawned at your position."),
    NPC_ERROR_NOSHOP("NPC_ERROR_NOSHOP", "&cThere is no shop called: %shop%. "),
    NPC_NOT_ENABLED("NPC_NOT_ENABLED", "&cCitizen support is not enabled."),
    NPC_REMOVE_CONFIRM("NPC_REMOVE_CONFIRM", "&cRight click again to remove %npcname%."),
    NPC_REMOVE_SUCCESS("NPC_REMOVE_SUCCESS", "&c%npcname% has been removed."),
    VOTE_MESSAGE("VOTE_MESSAGE", "&aYou have been rewarded %amount% Token(s) for voting! Thank you."),
    TOKEN_BALANCE("TOKEN_BALANCE", "&eYou have %token% Tokens."),
    TOKEN_BALANCE_OTHERS("TOKEN_BALANCE_OTHERS", "&e%target% has %token% token."),
    TOKEN_ADD("TOKEN_ADD", "&eAdded %token% Token to %player%'s balance."),
    TOKEN_REMOVE("TOKEN_REMOVE", "&eTook %token% Token from %player%'s Balance."),
    TOKEN_SEND("TOKEN_SEND", "&eYou have send %token% tokens to %player%"),
    TOKEN_RECEIVE("TOKEN_RECEIVE", "&eYou have received %token% tokens from %player%"),
    TOKEN_SET("TOKEN_SET", "&e%player%'s Token balance has been set to %token%"),
    TOKEN_NOT_ENOUGH("TOKEN_NOT_ENOUGH", "&cYou have not enough tokens."),
    TOKEN_WARNING("TOKEN_WARNING", "&cThis player is NOT online. Make sure you've spelt his nickname correctly"),
    TOKEN_TARGET_NOT_ENOUGH("TOKEN_TARGET_NOT_ENOUGH", "&c%player% does not have that many tokens."),
    FULL_INVENTORY("FULL_INVENTORY", "&cCannot complete purchase, your inventory is full"),
    NO_MONEY("NO_MONEY", "&cYou have not enough money."),
    PURCHASE_MONEY_COMPLETE("PURCHASE_MONEY_COMPLETE", "&aPaid %amount% %currencyname%"),
    NO_TOKEN("NO_TOKEN", "&cYou have not enough Token."),
    PURCHASE_TOKEN_COMPLETE("PURCHASE_TOKEN_COMPLETE", "&aPaid %amount% Token"),
    NO_ITEMS("NO_ITEMS", "&cYou have not enough %item% to buy this."),
    NO_LEVEL("NO_LEVEL", "&cYou have not enough XP level to buy this."),
    PURCHASE_LEVEL_COMPLETE("PURCHASE_LEVEL_COMPLETE", "&aPaid %level% Level(s)."),
    SELL_LEVEL_COMPLETE("SELL_LEVEL_COMPLETE", "&aGot %level% Level(s)."),
    SELL_MONEY_COMPLETE("SELL_MONEY_COMPLETE", "&aReceived %amount% %currencyname%"),
    COOLDOWN_MESSAGE("COOLDOWN_MESSAGE", "&6You can buy this package again in %seconds% seconds."),
    PERMISSION_BOUGHT("PERMISSION_BOUGHT", "&aPermission has been sucessfully added to your account."),
    NO_SLOT_PERMISSION("NO_SLOT_PERMISSION", "&cYou lack the proper permission to buy the item on this slot."),
    PERMISSION_ALREADY_BOUGHT("PERMISSION_ALREADY_BOUGHT", "&cThis permission is already assigned to your account.");
    private String path;
    private String def;
    private static YamlConfiguration LANG;
 
    /**
    * Lang enum constructor.
    * @param path The string path.
    * @param start The default string.
    */
    Lang(String path, String start) {
        this.path = path;
        this.def = start;
    }
 
    /**
    * Set the {@code YamlConfiguration} to use.
    * @param config The config to set.
    */
    public static void setFile(YamlConfiguration config) {
        LANG = config;
    }
 
    @Override
    public String toString() {
        if (this == TITLE)
            return ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, def)) + " ";
        return ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, def));
    }
 
    /**
    * Get the default value of the path.
    * @return The default value of the path.
    */
    public String getDefault() {
        return this.def;
    }
 
    /**
    * Get the path to the string.
    * @return The path to the string.
    */
    public String getPath() {
        return this.path;
    }
}
