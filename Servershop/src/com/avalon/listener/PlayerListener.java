package com.avalon.listener;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.avalon.servershop.ServerShop;

public class PlayerListener implements Listener {

	  public static ServerShop plugin;

	  public PlayerListener(ServerShop instance)
	  {
	    plugin = instance;
	  }
	  
	  @EventHandler
	  public void ShopOpenItem(PlayerInteractEvent event) {
			if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Player p = event.getPlayer();
				if (p.getItemInHand().getType() == Material.getMaterial(plugin.getConfig().getString("main.shopOpenWithItem").toUpperCase().replace(' ', '_'))) {
					if (plugin.getConfig().getBoolean("main.enableItem")) {
					p.openInventory(plugin.slist);
					return;
				}
				}
			}
	  }
}
