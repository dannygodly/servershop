package com.avalon.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.avalon.servershop.ServerShop;

public class TokenMySQL {
	
	public static ServerShop plugin;
	
	public TokenMySQL(ServerShop instance) {
		plugin = instance;
	}

	public static final String isInDB(String nick) {
		if (!plugin.checkConn()) {
			Bukkit.getPlayer(nick).sendMessage(ChatColor.RED + "Something went wrong - please try again.");
			return null;
		}
				try {
					Statement statement = plugin.c.createStatement();
					ResultSet r = statement.executeQuery("SELECT Token, Nickname FROM ServerShop WHERE Nickname = '" + nick + "';");
					String m = null;
					while (r.next()) {
						m = r.getString("Nickname");
						return m;
						 
					}
					return m;
					} catch (SQLException ex) {
					return null; }
	}
	
	
	  public static int getToken(String nickname) {
		  if (!plugin.checkConn()) {
				Bukkit.getPlayer(nickname).sendMessage(ChatColor.RED + "Something went wrong - please try again.");
				return 0;
			}
		   try { 
			   Statement statement = plugin.c.createStatement();
			   ResultSet r = statement.executeQuery("SELECT Token FROM ServerShop WHERE nickname = '" + nickname + "';");
			   int Token = 0;
			   while (r.next()) {
				   
				   Token =  r.getInt("Token");
				   return Token;
			   }
			   return 0;
			   
			   } catch (SQLException e) {
				   e.printStackTrace();
				   return 0;
			   }
	  }
	
	public static void setToken(final String p, final int amount) {
		if (!plugin.checkConn()) {
			Bukkit.getPlayer(p).sendMessage(ChatColor.RED + "Something went wrong - please try again.");
			return;
		}
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable(){	
			@Override
			public void run() {
				try {
				
					String pr;
				
					if (isInDB(p) == null) {
						pr = "INSERT INTO ServerShop (Token, Nickname) VALUES (?, ?);";
					} else {
						pr = "UPDATE ServerShop SET Token = ? WHERE Nickname = ?";
					}
				PreparedStatement stmt = plugin.c.prepareStatement(pr);
				stmt.setInt(1, amount);
				stmt.setString(2, p);
				stmt.executeUpdate();
				stmt.close();
				} catch (SQLException ex) {
					return;
				}
			}
			
		});
		
		
	}
	
	public static void sendTopTen(final Player p) {
		if (!plugin.checkConn()) {
			p.sendMessage(ChatColor.RED + "Something went wrong - please try again.");
			return;
		}
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable(){	
			@Override
			public void run() {
				try {
					
					   Statement statement = plugin.c.createStatement();
					   ResultSet r = statement.executeQuery("SELECT * FROM ServerShop ORDER BY Token DESC LIMIT 10;");
					   p.sendMessage(ChatColor.YELLOW + "Token top 10");
					   int counter = 1;
					   while (r.next()) {
						   p.sendMessage(ChatColor.WHITE + "" + counter + ": " + ChatColor.GOLD + r.getString("Nickname") + ChatColor.WHITE + " " +  r.getInt("Token") + " Tokens.");
						   counter++;
					   }
					
					
				} catch (SQLException ex) {
					return;
				}
			}
			
		});
		
		
	}
	
	  public static void giveToken(final String p, final int amount) {
		  if (!plugin.checkConn()) {
				Bukkit.getPlayer(p).sendMessage(ChatColor.RED + "Something went wrong - please try again.");
				return;
			}
			Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable(){	
				@Override
				public void run() {
					try {
					
						String pr;
						int balance = getToken(p);
						int newbalance = balance + amount;
						if (isInDB(p) == null) {
							pr = "INSERT INTO ServerShop (Token, Nickname) VALUES (?, ?);";
						} else {
							pr = "UPDATE ServerShop SET Token = ? WHERE Nickname = ?";
						}
					PreparedStatement stmt = plugin.c.prepareStatement(pr);
					stmt.setInt(1, newbalance);
					stmt.setString(2, p);
					stmt.executeUpdate();
					stmt.close();
					} catch (SQLException ex) {
						return;
					}
				}
				
			});
	  }
	  
	  public static boolean takeToken(final String p,final int amount) {
		  if (!plugin.checkConn()) {
				Bukkit.getPlayer(p).sendMessage(ChatColor.RED + "Something went wrong - please try again.");
				return false;
			}
					try {
					
						String pr;
						int balance = getToken(p);
						int newbalance = balance - amount;
						if (newbalance < 0) {
							return false;
						}
						if (isInDB(p) == null) {
							pr = "INSERT INTO ServerShop (Token, Nickname) VALUES (?, ?);";
						} else {
							pr = "UPDATE ServerShop SET Token = ? WHERE Nickname = ?";
						}
					PreparedStatement stmt = plugin.c.prepareStatement(pr);
					stmt.setInt(1, newbalance);
					stmt.setString(2, p);
					stmt.executeUpdate();
					stmt.close();
					return true;
					} catch (SQLException ex) {
						return false;
					}
				}

}
